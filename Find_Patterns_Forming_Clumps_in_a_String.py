from sets import Set

inp = open('rosalind_ba1e.txt', 'r')
genome = inp.readline().strip()
info = inp.readline().strip()

k_L_t = info.split(' ')

k = int(k_L_t[0])
L = int(k_L_t[1])
t = int(k_L_t[2])

length = len(genome)
kmer_list = {}
set_of_winners = Set([])

for num in range(0,L-k+1):
    current = genome[num:num+k]
    if current in kmer_list:
        kmer_list[current] += 1
    else:
        kmer_list[current] = 1
##    print kmer_list

##print length

for num in range(L-k+1,length-k+2):
##    print num
    current = genome[num:num+k]
    oldest = genome[num-L+k-1:num-L+k+k-1]
##    print oldest + str(num-L+k-1) + ", " + str(num-L+k+k-1)
    if kmer_list[oldest] >= t:
        set_of_winners.add(oldest)
    kmer_list[oldest] -= 1
    if current in kmer_list:
        kmer_list[current] += 1
    else:
        kmer_list[current] = 1

        
##ACGTACGT
        
for word in set_of_winners:
    print word