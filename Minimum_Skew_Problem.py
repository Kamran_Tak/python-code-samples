inp = open('rosalind_ba1f.txt', 'r')
genome = inp.readline().strip()
length = len(genome)
min_indices = []
min_skew = 0
current_skew = 0

for num in range(0,length):
    current = genome[num:num+1]
    if current is "C":
        current_skew -= 1
        if current_skew < min_skew:
            min_indices = [str(num+1)]
            min_skew = current_skew
        elif current_skew == min_skew:
            min_indices.append(str(num+1))
    elif current is "G":
        current_skew += 1
    elif current is "A" or current is "T":
        if current_skew == min_skew:
            min_indices.append(str(num+1))

min_indices_string = ""   
for index in min_indices:
    min_indices_string += index + " "

print min_indices_string