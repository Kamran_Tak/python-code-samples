# -*- coding: utf-8 -*-
"""
Created on Sun Mar 05 15:40:25 2017

@author: Kamran
"""

import sys
import random
import math

# set random number generator
# to get same result on each invocation
random.seed(2)

# utility structure
alphabet = list('ACGT')

# compute the entropy of a profile
def profile_entropy(profile, k):
    score = 0
    for j in xrange(k):
        cur_ent = 0
        for nuc in alphabet:
            cur_ent += -( profile[nuc][j] * math.log(profile[nuc][j], 2) )
        score += cur_ent
    return score

# construct a profile using pseudocounts
# 'indices': a list of starting indices for k-mers used to estimate profile
def make_profile(indices, dna, k):
    t = len(indices)
    # initialize profile
    profile = {}
    for nuc in alphabet:
        profile[nuc] = []

    # for each column in profile
    for i in xrange(k):
        # initialize counts with pseudocount = 1
        counts = dict.fromkeys(alphabet, 1.)

        # add count for each kmer
        for j in xrange(t):
            nuc = dna[j][indices[j]+i]
            counts[nuc] += 1

        # compute probability for each nucleotide
        for nuc, count in counts.iteritems():
            profile[nuc].append( count / ( t + len(alphabet)) )
    return profile


# compute the probability of given k-mer
# based on given profile
def profile_probability(kmer, profile):
    prob = 1.
    k = len(kmer)
    for i in xrange(k):
        prob *= profile[kmer[i]][i]
    return prob


# get kmers starting at each index from set of
# strings 'dna'
def get_kmers(indices, dna, k, t):
    kmers = [""] * t

    for i in xrange(t):
        start_index = indices[i]
        kmers[i] = dna[i][slice(start_index, start_index + k)]
    return kmers

# find one candidate profile
# using randomized search
def find_one_profile(k, t, N, dna):
    n = len(dna[0])

    # initialize to set of random indices
    indices = [random.randrange(n-k+1) for _ in xrange(t)]
    best_score = 1. * k * t

    # check if current set of indices can
    # be improved
    for _ in xrange(N):
        i = random.randrange(t-1)
        indices_without_row_i = indices[:i] + indices[i+1:]
        dna_without_row_i = dna[:i] + dna[i+1:]
        # construct a profile given current set of indices
        profile = make_profile(indices_without_row_i, dna_without_row_i, k)
        
        #find the probability of all kmers in row i
        kmer_prob_list = []
        probability_sum = 0
        for start in range(n-k+1):
            kmer_prob = profile_probability(dna[i][start:start+k], profile)
            kmer_prob_list.append(kmer_prob)
            probability_sum += kmer_prob
        for _ in xrange(n-k+1):
            kmer_prob_list[_] /= probability_sum
        
        indices[i] = profile_randomly_generated_kmer(kmer_prob_list)

    profile = make_profile(indices, dna, k)
    score = profile_entropy(profile, k)
# update minimum entropy if needed
    if score < best_score:
        best_score = score
    return best_score, profile, indices
    
def profile_randomly_generated_kmer(kmer_prob_list):
    random_kmer_selector = random.random() #random number from [0,1.0]
    running_prob_sum = 0.0
    kmer_start_index = 0
    while running_prob_sum < random_kmer_selector:
        running_prob_sum += kmer_prob_list[kmer_start_index]
        kmer_start_index += 1
    return kmer_start_index - 1

# outer loop for random motif search
# performs random search for 1000 random choices
def random_motif_search(k, t, N, dna):
    n = 20
    best_score = 1. * k * t
    best_indices = None

    for _ in xrange(n):
        cur_score, _, cur_indices = find_one_profile(k, t, N, dna)

        if cur_score < best_score:
            best_indices = cur_indices
            best_score = cur_score
    return get_kmers(best_indices, dna, k, t)

# read input and call search function
filename = 'input4.txt'
with open(filename, 'r') as f:
    k, t, N = map(int, f.readline().strip().split())
    dna = []
    for i in xrange(t):
        dna.append(f.readline().strip())
    
    print '\n'.join(random_motif_search(k, t, N, dna))