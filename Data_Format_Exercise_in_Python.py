from Bio import Entrez
from Bio import SeqIO
Entrez.email = "kamran.tak@gmail.com"
handle = Entrez.efetch(db="nucleotide", id=["JX428803 JF927165 BT149866 JX308821 NM_001131214 NM_001197168 JX462669 NM_001266228"], rettype="fasta")
records = list (SeqIO.parse(handle, "fasta")) #we get the list of SeqIO objects in FASTA format

shortest = 2**31 - 1

shortestIndex = 
num = len(records)
i = 0

while i < num:
    current = len(records[i].seq)
    if  current < shortest:
        shortestIndex = i
        shortest = current
    i += 1

print ">" + records[shortestIndex].description + "\n" + records[shortestIndex].seq